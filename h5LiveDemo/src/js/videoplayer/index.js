//import layout from '../layout';
require('./index.css');
export default class VideoPlayer
{
	constructor(props) {
		this.player;
		this.props = props;
		//this.props.isLive = true;
		this._setup();
		this._bindEvent();
		this.isFull = false;
		this.temCoverDom;
	}
	loadByUrl(url,cover_dom)
	{
		if(this.player){
			this.player.loadByUrl(url);
			this.coverBind(cover_dom);
		}
	}
	coverBind(cover_dom){
		let that = this;
		if(cover_dom){
			that.player.on("playing",function(){
				if(isAndroid()){
					//alert("andro....");
					that.temCoverDom = cover_dom;
					if(that.isFull){
						that.temCoverDom.style.display="none";
						that.player.off("playing");
					}
				}else{
					cover_dom.style.display="none";
					that.player.off("playing");
				}
			});
		}
	}
	dispose()
	{
		if(this.player)
		{
			this.player.dispose();
			Zepto('#'+this.props.id).empty();
		}
	}

    _setup()
	{
		this.player = new Aliplayer(this.props,function(player){
			 player._switchLevel = 0;
		});
	}

	_bindEvent()
	{
		var that = this;
		this.player.on('ready',  (e)=> {
            //console.log('ready');
			//$(".prism-big-play-btn").show();
			//that.player.play();
			//alert("ready>>>"+this.player.getStatus())
        });

        this.player.on('play',  (e)=>{
            //console.log('play');
			//var img =playSwiper.slides[1].firstChild;
			//img.src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==";
			//alert(this.player.getStatus());
        });

        this.player.on('ended',(e)=>{
            //console.log('ended');

            });
        this.player.on('pause',(e)=> {
            //console.log('pause');
			$(".prism-big-play-btn").show();
         });

        this.player.on('requestFullScreen', (e)=>{
        	//layout.adjustLayout(true);
        	that.player.cancelFullScreen();
			//that.player.height=window.screen.height+44+"px";
        });
       this.player.tag.addEventListener("x5videoenterfullscreen", ()=>{
        	that.isFull = true;
			if(that.isFull){
				that.temCoverDom.style.display="none";
				that.player.off("playing");
			}
			//alert(that.isFull);
			if(WeixinJSBridge){
        		//WeixinJSBridge.call('closeWindow');
			}
        });
        this.player.tag.addEventListener("x5videoexitfullscreen", ()=>{
			that.isFull = false;
        	if(WeixinJSBridge){
        		//WeixinJSBridge.call('closeWindow');
			}
        });

        // 解决ios不自动播放的问题
        $(document).on('WeixinJSBridgeReady',()=>{ 
		   var video=$(that.player.el()).find('video')[0];
		   video.play();
		});
	}

	_unbindEvent()
	{
		this.player.off('ready',function  (e) {
        console.log('ready');

        });

        this.player.off('play',function  (e) {
            console.log('play');

            });

        this.player.off('ended',function  (e) {
            console.log('ended');

            });
        this.player.off('pause',function  (e) {
            console.log('pause');

            });
	}

}
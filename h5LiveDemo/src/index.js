import VideoPlayer from './js/videoplayer';
//import Header from './js/header';
//import Comment from './js/comment';
//import Favoriate from './js/favoriate';
//import CommentSender from './js/commentsender';
import Loading from './js/loading';
//import util from './js/util';
//import layout from './js/layout';
require('./css/index.css');
//var comment, favoriate, header;
$(()=>{
	//doAliplay(null);
	if(isiOS()){
		$(".prism-player").addClass("ios");
	}
	/*else if(isAndroid()){
		$(".code-back").addClass("andr");
		//$('body').click();
	}*/
});
window.isiOS=function(){
	var u = navigator.userAgent;
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	return isiOS;
}	
window.isAndroid=function(){
	var u = navigator.userAgent;
    var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
	return isAndroid;
}	
/**
 * @param {Object} config
 * 	{reqUrl:分页数据请求地址}
 */
window.doAliplay=function(configParam){
	var defaultConfig ={};
	if(configParam!=null){
		$.extend(defaultConfig,configParam);
	}
	var linkUrl=defaultConfig.linkUrl;
    // 分页
    var page = 1;
	if(defaultConfig.page){
		page = defaultConfig.page;
	}
    var current = 0;
	if(defaultConfig.current){
		current=defaultConfig.current;
	}
    // 播放的列表
  var list = [];
  var first = true;
	var player ={};
	var activeNow = 0;
	var dataOver = false;
	var andrPhone = isAndroid();
	function init() {
    	console.log(">>>>>>get playInfo>>>>>>>>>>")
      var tem = getVideo();
			if(tem==null||tem.length==0){//无数据
				console.log("暂时无视频>>>>>");
				$(".swiper-container").hide();
				var blankDiv = ("<div class='blank'><div class='content'><span>暂无视频信息</span><br/><a href='javascript:history.go(-1)'>返回</a></div></div>");
				$('body').append(blankDiv);
				return ;	
			}
			$("body").click(function(){
				console.log("clicked>>>>");
				var temPlayer = player.player;
				var playToggle = temPlayer.getStatus() =="playing"||temPlayer.getStatus() =="ready";
				console.log("temPlayer.getStatus()>>>"+temPlayer.getStatus());
				if(playToggle){
					if(temPlayer.getCurrentTime()==0){
					 temPlayer.play();
					}else{
					temPlayer.pause();
					}
				}else{
					if(activeNow==0){
						var temSlider = playSwiper.slides[0];
						var img =temSlider.firstChild; 
						img.style.display='none';
					}
					temPlayer.play();
				}
			});
    }
    function getVideo() {
		var playInfos = [];
		 $.ajax({
			 type:'GET',
			  url:defaultConfig.reqUrl,
			  data:{page:page},
			  dataType:'json',
			  async:false,
			  success:function(res){
				  if(res==null||res.length==0){
					  dataOver=true;
					  return null;
				  }
				  playInfos = res;
				  handelRes(playInfos);
			  },error:function(){
				  
			  }
		  });
			/*
		  playInfos[0]={sName:"测试title测试title测试title测试title",videoUrl:"https://mp4.vjshi.com/2019-04-18/d3ade069a7167f837a1f1319b127cacf.mp4",descr:"test0",cover:"https://pic.vjshi.com/2018-12-11/a2c979bf32b6153e35dada582c62b4ae/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[1]={videoUrl:"https://mp4.vjshi.com/2019-04-18/d3ade069a7167f837a1f1319b127cacf.mp4",descr:"test1",cover:"https://pic.vjshi.com/2018-12-11/a2c979bf32b6153e35dada582c62b4ae/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[2]={videoUrl:"https://mp4.vjshi.com/2019-04-03/5983656f5ce44ae12186c053823f146e.mp4",descr:"test2",cover:"https://pic.vjshi.com/2019-06-20/e4c6c84427a943bf45db8847260c5956/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[3]={videoUrl:"https://lmp4.vjshi.com/2017-09-20/a45809c1300c824dd4704b158eb09854.mp4",descr:"test3",cover:"https://pic.vjshi.com/2019-06-19/006a93c616565f8c65195cb62dd92ea3/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[4]={videoUrl:"https://mp4.vjshi.com/2019-04-18/3463efeee4c82c00df4ba50355d8210d.mp4",descr:"test4",cover:"https://pic.vjshi.com/2019-06-19/006a93c616565f8c65195cb62dd92ea3/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[5]={videoUrl:"https://mp4.vjshi.com/2019-01-08/a42dad0cbe1307ac410d4c88db71a559.mp4",descr:"test5",cover:"https://pic.vjshi.com/2019-05-23/bcf5c05b1081c382c93cf966c13aaa86/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[6]={videoUrl:"https://mp4.vjshi.com/2018-08-29/f7fa93f4c61c6578b674c6ec55f4c9bf.mp4",descr:"test6",cover:"https://pic.vjshi.com/2019-05-23/bcf5c05b1081c382c93cf966c13aaa86/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
			playInfos[7]={videoUrl:"http://oss.kemiandan.com/miandan/test/20190417/2_3_4_0d57d80a-da93-4da9-aa9c-76888b5c994d.mp4",descr:"test7",cover:"https://pic.vjshi.com/2019-05-23/bcf5c05b1081c382c93cf966c13aaa86/online/puzzle.jpg?x-oss-process=style/resize_w_720"};
		  handelRes(playInfos);*/
		  return playInfos;
    }
    function handelRes(playInfos){
        list = list.concat(playInfos);
		/*
        if(list.length > 20) {
            var deleteLength = list.length - 20;
            list = list.splice(deleteLength, 20);
            current -= deleteLength;
        }*/
        if(first) {
            first = false;
						//Loading.show();
						if(playInfos==null||playInfos.length==0){
							return ; 
						}
						player = new VideoPlayer({
										id: 'prismPlayer1',
										autoplay: true,
										preload:true,
										isLive:false,
										rePlay:true,
										height:"100%",
										width:"100%",
										playsinline:true,
										source:list[current].videoUrl,
										useH5Prism:true,
										useFlashPrism:false,
										//cover: list[current].cover,
										 //prismplayer 2.0.1版本支持的属性，主要用户实现在android 微信上的同层播放
										x5_type:'h5', //通过 video 属性 “x5-video-player-type” 声明启用同层H5播放器，支持的值：h5 https://x5.tencent.com/tbs/guide/video.html
										x5_fullscreen:true,//通过 video 属性 “x5-video-player-fullscreen” 声明视频播放时是否进入到 TBS 的全屏模式，支持的值：true
										//x5_video_position:'center',
										skinLayout:[
											{name:"bigPlayButton", align:"blabs", x:"70", y:"150"},
											{name: "H5Loading", align: "cc"},
											{name: "errorDisplay", align: "tlabs", x: 0, y: 0},
											{name: "infoDisplay", align: "cc"},
										]
									});
        }
    }
	init();
	window.onresize = function(){
		setLayout();
	}
	var setLayout = function()
	{    
		//设置播放器容器的高度
		var height = window.screen.height; //根据实际情况设置高度
		$("#prismPlayer1").height(height);
		player.player.el().style.height = height;
	}
	//var opacity-img="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==";
	var playSwiper = new Swiper('.swiper-container',{
		direction: 'horizontal', // 水平切换选项
		loop:false,
		virtual:{
			  slides: list,
			  renderSlide:function(slide, index){
				  var imgSrc = slide.cover;
				  var display ="block";
				   if(index==0&&isiOS()){
					   display="none";
				   }
				   console.log("index>>>"+index);
				 var tempLink = linkUrl?linkUrl.replace("{id}",slide.sId):'';
				 
				 return '<div class="swiper-slide"><img  style="display:'+display+'" src="'+imgSrc+
						'" /><a class="code-back '+(andrPhone?'andr':'')+'" href="'+'javascript:history.go(-1);"'+'>X</a>'+
						'<div class="code-view ">'+
							'<div class="view-title"><a href="'+tempLink+'" class="video-title"><span class="text">@'+(!slide.sName?'':slide.sName)+'</span>&nbsp;<span class="detail-link" >查看详情</span></a></div>'+
							'<div class="video-desc">'+slide.descr+
							'</div>'+
						'</div>'+
					'</div>';
			  }
		},
		// allowSlidePrev:false,
		on:{
			slidePrevTransitionEnd: function(){
				console.log("slidePrev>>>");
				console.log("activeIndex>>>"+this.activeIndex);//切换结束时，告诉我现在是第几个slide
				if(this.activeIndex==activeNow){
					return;
				}
				current--;
				activeNow =this.activeIndex ;
				var temSlider = this.slides[1];
				if(this.activeIndex==0){
					temSlider =this.slides[0];
				}
				temSlider.style.opacity=1;
				var img =temSlider.firstChild;
				//img.style.display="none";
				player.loadByUrl(list[current].videoUrl,img);
				if(activeNow>=1){
					this.slides[0].firstChild.style.display="block";
					this.slides[2].firstChild.style.display="block";
				}else{
					this.slides[1].firstChild.style.display="block";
				}
				/*
				if(current-2<0){//重新取值
					if(page==0){
						return;
					}
					page--;
					var appendSliders =getVideo();
					if(appendSliders&&appendSliders.length>0){
						for(var temIndex in appendSliders){
							this.virtual.prependSlide(appendSliders[temIndex]);
						}
					}
				}*/
			},
			slideNextTransitionEnd: function(){
				console.log("slideNext>>>");
				console.log("activeIndex>>>"+this.activeIndex);//切换结束时，告诉我现在是第几个slide
				//console.log("active id>>>"+this.slides[this.activeIndex].id);//
				//alert("activeNow:"+activeNow+">>>activeIndex:"+this.activeIndex+">>>"+"current:"+current)
				if(this.activeIndex==activeNow){
					return;
				}
				current++;
				activeNow =this.activeIndex ;
				var temSlider = this.slides[1];
				temSlider.style.opacity=1;
				//player.player.pause();
				var img =temSlider.firstChild;
				//img.style.display="none";
				//img.src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==";
				player.loadByUrl(list[current].videoUrl,img);
				if(activeNow>=1){
					this.slides[0].firstChild.style.display="block";
					if(activeNow<list.length-1){
						this.slides[2].firstChild.style.display="block";
					}
				}
				if(current+2>list.length){//重新取值
					if(dataOver){
						return;
					}
					page++;
					var appendSliders =getVideo();
					if(appendSliders&&appendSliders.length>0){
						for(var temIndex in appendSliders){
							this.virtual.appendSlide(appendSliders[temIndex]);
						}
					}
				}
			}
		}
	});
	if(current>0){
		playSwiper.slideTo(current,0,false);
		var temSlider = playSwiper.slides[1];
		var img =temSlider.firstChild;
		console.log(">>"+playSwiper.activeIndex);
		player.coverBind(img);
	}
	//playSwiper.virtual.appendSlide('My Slide');
  
  /**
   * 
   * 获取下一个slider
   * 
   *	               2 0 1 2 0  loop循环 3个swapper item 会在首尾各添加一个
   *对应 activeIndex 为  0 1 2 3 4
   * 
   * next  2 3 4  pre 0 1 2
   * 
   * pre  4 --0  0--2  
   * 
   * next 4--2 
   * 
   * @param activeIndex
   * @param slideNext 时候向后滑动 
   * @returns
   */
  function getNextActiveIndex(activeIndex,slideNext){
  	var nextActiveIndex;
  	if(slideNext){// 向后滑动
  		if(activeIndex==4){
  			nextActiveIndex =2;
  		}else{
  			nextActiveIndex = activeIndex+1;
  		}
  	}else{// 向前滑动
  		if(activeIndex==4){
  			nextActiveIndex = 0;
  		}else if(activeIndex==0){
  			nextActiveIndex = 2;
  		}else{
  			nextActiveIndex = activeIndex-1;
  		}
  	}
  	return nextActiveIndex;
  }
}

(window.webpackJsonp = window.webpackJsonp || []).push([[1], [function(e, n, t) {
    "use strict";
    function i(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    t.r(n);
    var a = function() {
        function e() { !
            function(n, t) {
                if (! (n instanceof e)) throw new TypeError("Cannot call a class as a function")
            } (this)
        }
        var n, t;
        return n = e,
        t = [{
            key: "prefixedEvent",
            value: function(n, t, i) {
                for (var a = ["webkit", "moz", "MS", "o", ""], o = 0; o < a.length; o++) a[o] || (t = t.toLowerCase()),
                e.addEvent(n, a[o] + t, i)
            }
        },
        {
            key: "addEvent",
            value: function(e, n, t) {
                e.addEventListener && e.addEventListener(n, t, !1),
                e.attachEvent && e.attachEvent("on" + n, t)
            }
        },
        {
            key: "screenHeight",
            value: function() {
                return document.body.clientHeight || document.documentElement.clientHeight || window.screen.height || window.innerHeight
            }
        },
        {
            key: "isX5",
            value: function() {
                var e = navigator.userAgent;
                return /micromessenger/i.test(e) || /qqbrowser/i.test(e)
            }
        },
        {
            key: "encodeHtml",
            value: function(e) {
                return "string" != typeof e ? null: e.replace(/"|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g,
                function(e) {
                    var n = e.charCodeAt(0),
                    t = ["&#"];
                    return n = 32 == n ? 160 : n,
                    t.push(n),
                    t.push(";"),
                    t.join("")
                })
            }
        }],
        null && i(n.prototype, null),
        t && i(n, t),
        e
    } ();
    function o(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    var r = function() {
        function e() { !
            function(e, n) {
                if (! (e instanceof n)) throw new TypeError("Cannot call a class as a function")
            } (this, e)
        }
        var n, t;
        return n = e,
        t = [{
            key: "adjustLayout",
            value: function() {
                var e = 0 < arguments.length && void 0 !== arguments[0] && arguments[0],
                n = a.screenHeight(),
                t = $(".comment-textbox"),
                i = t.height() + 18;
                e && (i = -1 * i + 5),
                t.css("top", n - i);
                var o = $(".comment-list"),
                r = o.height();
                o.css("top", n - r - i);
                var s = $(".favorite-animation-container"),
                u = s.height();
                s.css("top", n - u - i)
            }
        }],
        null && o(n.prototype, null),
        t && o(n, t),
        e
    } ();
    function s(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    t(2);
    var u = function() {
        function e(n) { !
            function(e, n) {
                if (! (e instanceof n)) throw new TypeError("Cannot call a class as a function")
            } (this, e),
            this.player,
            this.props = n,
            this.props.isLive = !0,
            this._setup(),
            this._bindEvent()
        }
        var n, t;
        return n = e,
        (t = [{
            key: "loadByUrl",
            value: function(e) {
                this.player && this.player.loadByUrl(e)
            }
        },
        {
            key: "dispose",
            value: function() {
                this.player && (this.player.dispose(), Zepto("#" + this.props.id).empty())
            }
        },
        {
            key: "_setup",
            value: function() {
                this.player = new Aliplayer(this.props,
                function(e) {
                    e._switchLevel = 0
                })
            }
        },
        {
            key: "_bindEvent",
            value: function() {
                var e = this;
                this.player.on("ready",
                function(e) {}),
                this.player.on("play",
                function(e) {}),
                this.player.on("ended",
                function(e) {}),
                this.player.on("pause",
                function(e) {}),
                this.player.on("requestFullScreen",
                function(n) {
                    r.adjustLayout(!0),
                    e.player.cancelFullScreen()
                }),
                this.player.tag.addEventListener("x5videoexitfullscreen",
                function() {
                    WeixinJSBridge && WeixinJSBridge.call("closeWindow")
                }),
                $(document).on("WeixinJSBridgeReady",
                function() {
                    $(e.player.el()).find("video")[0].play()
                })
            }
        },
        {
            key: "_unbindEvent",
            value: function() {
                this.player.off("ready",
                function(e) {}),
                this.player.off("play",
                function(e) {}),
                this.player.off("ended",
                function(e) {}),
                this.player.off("pause",
                function(e) {})
            }
        }]) && s(n.prototype, t),
        e
    } ();
    function c(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    var l = {
        Comments_Sended: "commentSended",
        Favoriation_Sended: "FavoriationSended",
        Viwer_Added: "viwerAdded"
    },
    f = function() {
        function e() { !
            function(e, n) {
                if (! (e instanceof n)) throw new TypeError("Cannot call a class as a function")
            } (this, e)
        }
        var n, t;
        return n = e,
        t = [{
            key: "on",
            value: function(e, n) {
                $(document).on(e, n)
            }
        },
        {
            key: "trigger",
            value: function(e, n) {
                $(document).trigger(e, n)
            }
        },
        {
            key: "off",
            value: function(e, n) {
                $(document).off(e, n)
            }
        },
        {
            key: "EventConstant",
            get: function() {
                return l
            }
        }],
        null && c(n.prototype, null),
        t && c(n, t),
        e
    } ();
    function d(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    t(6);
    var v = function() {
        function e() { !
            function(n, t) {
                if (! (n instanceof e)) throw new TypeError("Cannot call a class as a function")
            } (this),
            this.backup = [],
            this.max_count = 5
        }
        var n, t, i;
        return n = e,
        i = [{
            key: "_createImgHeader",
            value: function(e, n, t) {
                var i = $('<img  class="audience-header">');
                i.attr("src", n),
                i.attr("data-uid", t),
                e.prepend(i)
            }
        },
        {
            key: "computeLeft",
            value: function(e, n) {
                var t = e.width() + 20 / 68 * window.rem;
                n.css("left", t)
            }
        }],
        (t = [{
            key: "setup",
            value: function(e) {
                var n = $(".video-header");
                n.find(".user-name").text(e.nickName),
                n.find(".user-head-img").attr("src", e.avatar),
                this._updateLikeNumber(e.likeNum),
                this._updateViewNumber(e.viewerNum),
                this._getAudiences();
                var t = this;
                f.on(f.EventConstant.Favoriation_Sended,
                function() {
                    t._updateLikeNumber(1)
                }),
                f.on(f.EventConstant.Viwer_Added,
                function() {
                    t._updateViewNumber(1)
                })
            }
        },
        {
            key: "_updateLikeNumber",
            value: function(e) {
                var n = $(".video-header .user-favorite"),
                t = parseInt(n.text());
                n.text(t + e)
            }
        },
        {
            key: "_updateViewNumber",
            value: function(n, t) {
                if (null != n) {
                    var i = $(".video-header span.audience-count"),
                    a = parseInt(i.text());
                    i.text("".concat(a + n, "人"))
                }
                var o = $(".audience-detail");
                if (e.computeLeft($(".video-header .author"), o), t) if (0 === t.type) this.isFullHeaders ? ($(".audience-detail .ellipsis").addClass("show"), this.backup.push(t)) : (e._createImgHeader(o, t.avatar, t.uid), this.isFullHeaders = o.find(".audience-header").length == this.max_count);
                else if (0 < this.backup.length) {
                    var r = this.backup.pop(),
                    s = o.find('img[data-uid="' + t.uid + '"]');
                    s.attr("src", r.avatar),
                    s.attr("data-uid", r.uid)
                } else o.find('img[data-uid="' + t.uid + '"]').remove(),
                $(".audience-detail .ellipsis").addClass("hide"),
                this.isFullHeaders = !1
            }
        },
        {
            key: "_getAudiences",
            value: function(n, t) {
                for (var i = 1; i < 10; i++) e._createImgHeader($(".audience-detail"), "./images/avatar.jpg", i)
            }
        }]) && d(n.prototype, t),
        i && d(n, i),
        e
    } ();
    function m(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    var h = function() {
        function e(n) { !
            function(e, n) {
                if (! (e instanceof n)) throw new TypeError("Cannot call a class as a function")
            } (this, e),
            this.createEl(),
            this._setupComment(),
            this.maxTopRow = null,
            this.colorIndex = 1
        }
        var n, t;
        return n = e,
        (t = [{
            key: "createEl",
            value: function(e) {
                this.comments = $(".comment-list"),
                this.container = this.comments.find(".comment-container")
            }
        },
        {
            key: "clear",
            value: function() {
                this.comments.empty()
            }
        },
        {
            key: "isFull",
            value: function() {
                return 0 == this.comments.find(".empty-comment-row").length
            }
        },
        {
            key: "send",
            value: function(e) {
                var n, t = this.comments.children(".comment-row");
                e && (n = 0 < t.length ? $(t[0]) : this._createEmptyRow(), this._assignValues(n, e), this._move(n))
            }
        },
        {
            key: "_move",
            value: function(e) {
                e.show();
                var n = this.comments.height(),
                t = this.container.height();
                e.height(),
                this.container.append(e);
                var i = this.container.children().first();
                t - n > i.height() && (this._removeClass(i.find(".comment-right")), i.hide(), this.comments.append(i))
            }
        },
        {
            key: "_assignValues",
            value: function(e, n) {
                e.find(".comment-left").text(n.name);
                var t = e.find(".comment-right");
                t.html(n.comment),
                t.addClass(this._getClass())
            }
        },
        {
            key: "_createEmptyRow",
            value: function() {
                var e = $('<div class="comment-row"><span class="comment-left"></span><span class="comment-right"></div>');
                return e.hide(),
                this.comments.append(e),
                e
            }
        },
        {
            key: "_setupComment",
            value: function() {
                for (var e = 0; e < 20; e++) this._createEmptyRow()
            }
        },
        {
            key: "_getClass",
            value: function(e) {
                4 < this.colorIndex && (this.colorIndex = 1);
                var n = "comment-color" + this.colorIndex;
                return this.colorIndex++,
                n
            }
        },
        {
            key: "_removeClass",
            value: function(e) {
                e.removeClass("comment-color1"),
                e.removeClass("comment-color2"),
                e.removeClass("comment-color3"),
                e.removeClass("comment-color4")
            }
        }]) && m(n.prototype, t),
        e
    } ();
    function p(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    t(10);
    var y = function() {
        function e(n, t) {
            var i = this; !
            function(n, t) {
                if (! (n instanceof e)) throw new TypeError("Cannot call a class as a function")
            } (this),
            this.comments = [],
            this.liveComment = new h(n, t),
            f.on(f.EventConstant.Comments_Sended,
            function(e, n) {
                i.add(n)
            })
        }
        var n, t;
        return n = e,
        (t = [{
            key: "clear",
            value: function() {
                this.stop(),
                this.liveComment.clear()
            }
        },
        {
            key: "add",
            value: function(e) {
                this.isStop || (this.comments.push(e), this.isWorking || this._wake())
            }
        },
        {
            key: "stop",
            value: function() {
                this.comments = [],
                this.isWorking = !1,
                this.isStop = !0,
                clearInterval(this.interval)
            }
        },
        {
            key: "start",
            value: function() {
                this.isStop = !1
            }
        },
        {
            key: "_wake",
            value: function() {
                this.isWorking = !0,
                this.interval = setInterval(this._handle(), 500)
            }
        },
        {
            key: "_handle",
            value: function() {
                var e = this;
                return function() {
                    if (0 < e.comments.length) {
                        e.liveComment.isFull() && e.liveComment.send("");
                        var n = e.comments.shift();
                        e.liveComment.send(n)
                    } else e.isWorking = !1,
                    clearInterval(e.interval)
                }
            }
        }]) && p(n.prototype, t),
        e
    } ();
    function g(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    t(12);
    var b = function() {
        function e() {
            var n = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : ["xin.png", "xin.png", "xin.png", "xin.png", "xin.png", "xin.png"]; !
            function(n, t) {
                if (! (n instanceof e)) throw new TypeError("Cannot call a class as a function")
            } (this),
            this.animateContainer = $(".favorite-animation-container"),
            this.imgNames = n,
            this.index = 0;
            var t = this;
            f.on(f.EventConstant.Favoriation_Sended,
            function() {
                t.favoriate()
            })
        }
        var n, t;
        return n = e,
        (t = [{
            key: "favoriate",
            value: function() {
                var e = this.imgNames.length;
                this.index = this.index < e ? this.index: 0,
                name = this.imgNames[this.index];
                var n = $('<img src="./images/'.concat(name, '" class="favorite-animation">'));
                this.animateContainer.append(n),
                this.index++,
                a.prefixedEvent(n[0], "animationend",
                function() {
                    n.remove()
                })
            }
        }]) && g(n.prototype, t),
        e
    } ();
    function w(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    t(14);
    var k = function() {
        function e() { !
            function(e, n) {
                if (! (e instanceof n)) throw new TypeError("Cannot call a class as a function")
            } (this, e)
        }
        var n, t;
        return n = e,
        t = [{
            key: "setup",
            value: function() {
                $(".comment-textbox .send-btn").click(function() {
                    var e = $(".comment-textbox .send-txt"),
                    n = e.val();
                    f.trigger(f.EventConstant.Comments_Sended, {
                        name: "小鱼儿",
                        comment: a.encodeHtml(n)
                    }),
                    e.val("")
                }),
                $(".comment-textbox .favoriate-send").click(function() {
                    f.trigger(f.EventConstant.Favoriation_Sended)
                })
            }
        }],
        null && w(n.prototype, null),
        t && w(n, t),
        e
    } ();
    function x(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value" in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    var _ = function() {
        function e() { !
            function(e, n) {
                if (! (e instanceof n)) throw new TypeError("Cannot call a class as a function")
            } (this, e)
        }
        var n, t;
        return n = e,
        t = [{
            key: "show",
            value: function() {
                var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : 0;
                0 != e && $(".loading-container").css("opacity", e),
                $(".loading-container").addClass("show")
            }
        },
        {
            key: "hide",
            value: function() {
                $(".loading-container").css("opacity", 0),
                $(".loading-container").addClass("hide")
            }
        }],
        null && x(n.prototype, null),
        t && x(n, t),
        e
    } ();
    t(16),
    $(function() {
        new u({
            id: "J_prismPlayer",
            autoplay: !0,
            isLive: !0,
            height: "100%",
            width: "100%",
            playsinline: !0,
            source: "https://player.alicdn.com/resource/player/big_buck_bunny.mp4",
            useH5Prism: !0,
            useFlashPrism: !1,
            cover: "http://a.hiphotos.baidu.com/image/pic/item/8326cffc1e178a82f973fe23f803738da877e8e2.jpg",
            x5_type: "h5",
            x5_fullscreen: !0,
            skinLayout: [{
                name: "bigPlayButton",
                align: "blabs",
                x: "70",
                y: "150"
            },
            {
                name: "H5Loading",
                align: "cc"
            },
            {
                name: "errorDisplay",
                align: "tlabs",
                x: 0,
                y: 0
            },
            {
                name: "infoDisplay",
                align: "cc"
            }]
        }),
        k.setup(),
        new y,
        new b,
        (new v).setup({
            nickName: "小鱼儿",
            avatar: "./images/avatar.jpg",
            likeNum: 2346,
            viewerNum: 12390
        }),
        r.adjustLayout(),
        _.hide()
    })
},
function(e, n, t) {
    e.exports = t(0)
},
function(e, n) {},
, , ,
function(e, n) {},
, , ,
function(e, n) {},
,
function(e, n) {},
,
function(e, n) {},
,
function(e, n) {}]]);